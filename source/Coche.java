
import javax.swing.JOptionPane;

public class Coche {

	private String matricula = "";
	private String marca = "Seat";
	private String modelo = "Altea";
	private String color = "Blanco";
	private boolean techoSolar = false;
	private double kilometros = 0;
	private int numPuertas = 3;
	private int numPlazas = 5;
	
	public Coche() {//constructor defecto
		
		
	}
	
	public Coche(String matric) {
		
		matricula = matric;
		
	}
	
	public Coche(int puerta2, int plaza2) {
	
		numPuertas = puerta2;
		numPlazas = plaza2;
	}
	
	public Coche(String marca2, String modelo2, String color2) {
		
		marca = marca2;
		modelo = modelo2;
		color = color2;
		
	}
	
	public void configMarca(String marca1) {//setter
		
		marca = marca1;
	}
	
	public String dimeMarca() {//getter
		
		return marca;
	}
	
	public void configModelo(String model) {//setter
		
		modelo = model;
	}
	
	public String dimeModelo() {//getter
		
		return modelo;
	}
	
	public void configColor(String color1) {//setter
		
		color = color1;
	}
	
	public String dimeColor() {//getter
		
		return color;
	}
	
	public void configMatricula(String placa) {//setter
		
		matricula = placa;
	}
	
	public String dimeMatricula() {//getter
		
		return matricula;
	}
	
public void configTecho(String solar) {//setter
		
		if(solar.equalsIgnoreCase("si")) {
			
			techoSolar = true;
		}
		
		else{
			techoSolar = false;
		}
		
	}
	
	public String dimeTecho() {//getter
		
		if(techoSolar == true) {
			
			return "si";
		}
		else {
			
			return "no";
		}
	}
	
	public void configPuertas(int puertas) {//setter
		
		if(puertas <= 5 && puertas >= 2) {
	
			numPuertas = puertas;
		}
	}

	public int dimePuertas() {//getter
	
		return  numPuertas;
	}
	
	public void configPlazas(int plazas1) {//setter
		
		if(plazas1 <= 7 && plazas1 >= 2) {
			
			numPlazas = plazas1;
		}
	}
	
	public int dimePlazas() {//getter
	
		return numPlazas;
	}

	public void configKilometros(double kilo) {
		
		kilometros = kilo;
	}
	
	public double dimeKilometros() {
		
		return kilometros;
	}
	
	public void avanzar(double kilometro1) {
		
		configKilometros(dimeKilometros()+kilometro1);
		JOptionPane.showMessageDialog(null, "El mecánico ha hecho avanzar " + kilometro1 + "km. Y los kilometros totales del coche son: " + dimeKilometros());
	}
	
	public void matricular(String placa) {
		
		configMatricula(placa);
		JOptionPane.showMessageDialog(null, "Se ha matriculado el coche con el codigo " + placa);
	}
	
	public void tunear() {
		
		configKilometros(0);
		configTecho("si");
		JOptionPane.showMessageDialog(null, "Se ha dejado el cuentakilometros a " + dimeKilometros() + " km. Y se le ha puesto techo solar");
	}
	
	public void tunear(String color1) {
		
		configKilometros(0);
		configTecho("si");
		configColor(color1);
		JOptionPane.showMessageDialog(null, "Se ha cambiado el color del coche a " + color1 + ".Y se ha dejado el cuentakilometros a cero y se le ha instalado un techo solar al coche");
	}

}

