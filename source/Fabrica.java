
import javax.swing.JOptionPane;

public class Fabrica {

	public static void main(String[] args) {
		
		Coche car1 = new Coche();
		car1.matricular("1234-DF");
		car1.configModelo("Toledo");
		car1.configColor("Azul");
		car1.avanzar(200);
		
		Coche car2 = new Coche("5678-AG");
		car2.configMarca("Fiat");
		car2.configModelo("Uno");
		car2.tunear("Rojo");
		car2.avanzar(300);
		car2.configPlazas(2);
		
		Coche car3 = new Coche(5, 5);
		car3.matricular("9012-HH");
		car3.configMarca("BMW");
		car3.configModelo("850");
		car3.configColor("Gris");
		car3.avanzar(400);
	
		Coche car4 = new Coche("VW", "Caddy", "Blanco");
		car4.matricular("3456-XS");
		car4.tunear();
		car4.avanzar(500);
		car4.configPuertas(5);
		car4.configPlazas(7);
		
		Coche []car = new Coche[4];
		
		//almacenamos cada coche en una posicion del array
		car[0] = car1; 
		
		car[1] = car2;
	
		car[2] = car3;
		
		car[3] = car4;
		
		//creamos un bucle for para recorrer el array
		for(Coche element:car) {
			
			Fabrica.caracteristicas(element);
				
		}
	}
	
	public static void caracteristicas(Coche a) {
	
		String mensaje = "La matricula del coche es: " + a.dimeMatricula() + "\n" + "La marca del coche es: " + a.dimeMarca() + "\n" + "El modelo del coche es: " + a.dimeModelo() + "\n" + "El color del coche es: " + a.dimeColor() + "\n" + "Techo solar: " + a.dimeTecho() + "\n"+"Tiene: " + a.dimeKilometros() + "km" + "\n"+"Tiene: " + a.dimePuertas() +" puertas"  + "\n" +"Tiene: " + a.dimePlazas() + " plazas";
		JOptionPane.showMessageDialog(null, mensaje);
	}
}

